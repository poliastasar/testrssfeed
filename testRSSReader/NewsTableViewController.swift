import UIKit

class NewsTableViewController: UITableViewController {

    private var rssItems = [RSSItem]()
    private var cellStates = [CellState]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 155.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        fetchData()
    }
    
    private func fetchData() {
        
        DispatchQueue.global().async {
            self.fetchDataFromLentaSync()
            self.fetchDataFromGazetaSync()
            
            self.rssItems = self.rssItems.sorted(by:  { $0.pubDate < $1.pubDate })
            self.cellStates = Array(repeating: .collapsed, count: self.rssItems.count)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func fetchDataFromLentaSync() {        
        let semaphore = DispatchSemaphore(value: 0)
        let feedParser = FeedParser()
        feedParser.parseFeed(url: "http://lenta.ru/rss") { (rssItems) in
            self.rssItems.append(contentsOf: rssItems)
            semaphore.signal()
        }
        semaphore.wait()
    }
    
    private func fetchDataFromGazetaSync() {
        let semaphore = DispatchSemaphore(value: 0)
        let feedParser = FeedParser()
        feedParser.parseFeed(url: "http://www.gazeta.ru/export/rss/lenta.xml") { (rssItems) in
            self.rssItems.append(contentsOf: rssItems)
            semaphore.signal()
        }
        semaphore.wait()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NewsTableViewCell
        let item = rssItems[indexPath.item]
        
        cell.item = item
        cell.selectionStyle = .none
        cell.descriptionLabel.text = (cellStates[indexPath.row] == .expanded) ? item.description : nil

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! NewsTableViewCell
        let item = rssItems[indexPath.item]
        
        tableView.beginUpdates()
        
        cell.descriptionLabel.text = (cellStates[indexPath.row] == .expanded) ? item.description : nil

        switch cellStates[indexPath.row] {
        case .expanded:
            cellStates[indexPath.row] = .collapsed
        case .collapsed:
            cellStates[indexPath.row] = .expanded
        default:
            break
        }
        
        tableView.endUpdates()
    }
}
