//
//  NewsTableViewCell.swift
//  testRSSReader
//
//  Created by Admin on 25.06.2018.
//  Copyright © 2018 RP. All rights reserved.
//

import UIKit

enum CellState {
    case expanded
    case collapsed
}

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var item: RSSItem! {
        didSet {
            titleLabel.text = item.title
            dateLabel.text = item.pubDate
            descriptionLabel.text = item.description
            
            if item.source == "http://lenta.ru/rss" {
                sourceLabel.text = "lenta.ru"
            }
            else {
                sourceLabel.text = "gazeta.ru"
            }
            if let image = item.image {
                downloadImage(url: image)
            }
        }
    }
    
    func downloadImage(url: URL) {
        let request = URLRequest(url: url)
        let urlSession = URLSession.shared
        
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            DispatchQueue.main.async {
                self.newsImage.image = UIImage(data: data)
            }
        }
        task.resume()
    }
}
